package com.casseteteam.gaara;


import java.util.ArrayList;

/**
 * Created by gaara on 6/18/16.
 */
public class DynamicStringList implements SimpleList{
    private String[] strs;
    private int count;
    DynamicStringList()
    {

        strs = new String[0];
        count = 0;
    }
    DynamicStringList(int cnt)
    {

        strs = new String[cnt];
        count = cnt;
    }
    public int Size()
    {
        return this.count;
    }
    public void add(String s)
    {

        String[] newS = new String[++count];
        for (int i=0;i<count - 1;i++)
        {
            newS[i] = strs[i];
        }
        newS[count-1] = new String(s);
        strs = newS;

    }
    public String get()
    {
        try {
            return strs[count-1];
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return null;
        }

    }
    public String get(int id)
    {
        return strs[id];
    }
    public void remove()
    {
        strs[count-1] = null;
        count--;
    }
    public void remove(int id)
    {

        for (int i = id;i<count-1;i++)
        {
            strs[i] = strs[i + 1];
        }
        count--;

    }
    public boolean delete()
    {
        for (int i=0;i<count;i++)
        {
            strs[i] = null;
        }
        count = 0;
        return true;
    }
    @Override
    public String toString()
    {
        if (this.count == 0)
            return "null";

        String result = new String();
        for (int i=0;i<this.count;i++)
        {
            result += "[" + strs[i] + "]";
            if(i != this.count-1)
                result += ',';
        }
        return result;
    }
}
