package com.casseteteam.gaara;

/**
 * Created by gaara on 20.06.16.
 */
public interface SimpleList {
    public int Size();
    public void add(String s);
    public String get();
    public String get(int id);
    public void remove();
    public void remove(int id);
    public boolean delete();
}
