package com.casseteteam.gaara;

/*
1. Создать интерфейс SimpleList, который определяет методы динамического массива, предназначенного для хранения строк:
- void add(String s) - добавить строку в массив
- String get() - получить последний элемент из массива
- String get(int id) - получить элемент по его индексу
- String remove() - удалить последний элемент
- String remove(int id) - удалить элемент по индексу
- boolean delete() - удалить все элементы массива.
2. Создать класс DynamicStringList, который реализует все методы интерфейса SimpleList. Для хранения элементов добавленных в объект типа DynamicStringList должен использоваться обычный Java-массив.
3. В классе DynamicStringList предусмотреть 2 конструктора: а)пустой и б) с числовым аргументом, определяющим начальный размер массива для хранения строк.
4. Все поля, используемые в классе для хранения элементов массива должны быть инкапсулированы.
5. Переопределить метод toString этого класса, для вывода всех строк, которые хранятся в объекте типа DynamicStringList
6. Создать объект типа DynamicStringList. Продемонстрировать добавление, извлечение, удаление строк из объекта, а также вывод на экран его содержимого.
5. На этапе составления программы должны быть использованы соглашения из java code convention.
6. Выполненное задание переслать в SVN репозатарий по адресу http://oracle-academy.org.ua/svn/<Имя проекта>/<Jira login>
*/



public class Main {
    //Пример использование Simple List
    public static void DSLExample()
    {
        DynamicStringList sl = new DynamicStringList();
        DynamicStringList sl2 = new DynamicStringList(3);
        System.out.println("SL2[0] =" + sl2.get(0));
        System.out.println("SL2[1] =" + sl2.get(1));
        System.out.println("SL2[2] =" + sl2.get(2));
        System.out.println("---------------------------");



        sl.add(new String("Hey"));
        sl.add(new String("Hey1"));
        sl.add(new String("Hey2"));


        System.out.println("0 =" + sl.get(0));
        System.out.println("1 =" + sl.get(1));
        System.out.println("2 =" + sl.get(2));
        System.out.println("last =" + sl.get());
        System.out.println("Size = " + sl.Size());
        System.out.println("---------------------------");
        sl.remove();
        sl.remove(0);


        System.out.println("0 =" + sl.get(0));
        System.out.println("1 =" + sl.get(1));
        System.out.println("2 =" + sl.get(2));
        System.out.println("last =" + sl.get());
        System.out.println("Size = " + sl.Size());
        System.out.println("---------------------------");
        sl.add("Another 1");
        sl.add("Another 2");
        sl.add("Another 3");
        sl.add("Another 4");
        sl.add("Another 5");

        sl.remove(4);
        for (int i=0;i<sl.Size();i++)
            System.out.println(i + " = " + sl.get(i));

        System.out.println("last =" + sl.get());
        System.out.println("Size = " + sl.Size());
        System.out.println("---------------------------");

        System.out.println(sl.toString());
        System.out.println("---------------------------");

        sl.delete();

        for (int i=0;i<sl.Size();i++)
            System.out.println(i + " = " + sl.get(i));

        System.out.println("last =" + sl.get());
        System.out.println("Size = " + sl.Size());


    }
    public static void main(String[] args)
    {
        DSLExample();

    }
}
